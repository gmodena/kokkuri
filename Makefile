.PHONY: lint test

lint test:
	docker run --rm -it $(shell DOCKER_BUILDKIT=1 docker build -qf .pipeline/blubber.yaml --target $@ .)

_lint:
	shellcheck lib/*/**.sh bin/kokkuri

_test:
	prove test/test-*.sh

image:
	DOCKER_BUILDKIT=1 docker build -qf .pipeline/blubber.yaml --target kokkuri -t kokkuri .

run: image
	docker run --rm -it kokkuri

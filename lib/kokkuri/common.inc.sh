#!/bin/bash
#
# Common kokkuri functions
#

: "${KOKKURI_DOTENV_PATH:=".kokkuri.env"}"
: "${KOKKURI_VERBOSITY:=2}"

# Print usage statements
#
usage() {
  local mods
  mapfile -t mods < <(modules)

  echo -n "Usage: $SCRIPT {--verbose|--quiet} "

  if [ $# -gt 0 ]; then
    for line in "${@}"; do
      echo "$line"
    done
  else
    echo "{$(join "|" "${mods[@]}")}"
  fi

  exit 2
}

# Saves a GitLab dotenv report for the given variable and value. The report
# can then be saved as an artifact and imported by subsequent stages where the
# variables may be utilized within job fields that support variable expansion.
#
# Note that the variable name will be prefixed by the name of the current job.
#
# Example (in job FOO_JOB):
#
#   export_job_variable KEY value
#   cat .kokkuri.env # FOO_JOB_KEY=value
#
export_job_variable() {
  local name="$1"
  local value="$2"
  local prefix fullname

  prefix="$(echo -n "${CI_JOB_NAME}" | tr '[:lower:]' '[:upper:]' | tr -C '[:alnum:]' _)"
  fullname="${prefix}_${name}"

  debug "Saving variable '${fullname}' with value '${value}' to dotenv report ${KOKKURI_DOTENV_PATH}"

  echo "${fullname}=${value}" >> "${KOKKURI_DOTENV_PATH}"
}

# Logs info to the console about exported variables that can be used in
# subsequent pipeline stages.
#
log_exported_variables() {
  if ! [ -e "${KOKKURI_DOTENV_PATH}" ]; then
    return
  fi

  info "Exported the following variables for use by subsequent stages"

  if [ "$KOKKURI_VERBOSITY" -ge 2 ]; then
    >&2 cat "${KOKKURI_DOTENV_PATH}"
  fi
}

# Available kokkuri modules
#
modules() {
  find "$LIBDIR" -name "*.mod.sh" -exec basename {} .mod.sh \;
}

# Join arguments by a delimiter
# See https://stackoverflow.com/questions/1527049/how-can-i-join-elements-of-an-array-in-bash
join() {
  local delim="${1-}" first="${2-}"

  if shift 2; then
    printf %s "$first" "${@/#/$delim}"
  fi
}

# Log to stderr at a particular logging level. If KOKKURI_VERBOSITY is equal to or
# higher than the level, the message is logged.
#
log() {
  local level=$1
  shift

  if [ "$level" -le "$KOKKURI_VERBOSITY" ]; then
    >&2 echo "$@"
  fi
}

# Log message at log level 1
#
warn() {
  log 1 "$@"
}

# Log message at log level 2
#
info() {
  log 2 "$@"
}

# Log message at log level 3
#
debug() {
  log 3 "$@"
}

# Conditionally echos the second argument only if the first is non-empty
#
echo_if() {
  local condition="$1"
  shift

  [ "$condition" ] && echo "$@" || echo ""
}

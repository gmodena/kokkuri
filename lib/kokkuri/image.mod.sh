#!/bin/bash
#
# Kokkuri functions for building and publishing images.
#

KOKKURI_DEFAULT_BUILDKIT_FRONTEND=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.16.0

: "${KOKKURI_REGISTRY_INTERNAL:=""}"
: "${KOKKURI_REGISTRY_INTERNAL_NS:="${KOKKURI_REGISTRY_INTERNAL}/${CI_PROJECT_PATH}"}"

: "${KOKKURI_REGISTRY_PUBLIC:=""}"
: "${KOKKURI_REGISTRY_PUBLIC_NS:="${KOKKURI_REGISTRY_PUBLIC}/${CI_PROJECT_PATH}"}"

: "${KOKKURI_REGISTRY_CACHE:=""}"
: "${KOKKURI_REGISTRY_CACHE_NS:="${KOKKURI_REGISTRY_CACHE}/${CI_PROJECT_PATH}/cache"}"

: "${BUILDCTL_FLAGS:=""}"
: "${BUILDCTL_BUILD_FLAGS:=""}"
: "${DOCKER_CONFIG:="${HOME}/.docker"}"

kokkuri_image_usage() {
  cat <<EOF
kokkuri image subcommands:
 kokkuri image build ...
 kokkuri image deploy ...
EOF
}

kokkuri_image_build_usage() {
  usage "image build [options] variant -- [buildctl arguments]" \
    "  --context                   Build directory sent to buildkit (Default '.')" \
    "  --config                    Build config (Default '.pipeline/blubber.yaml')" \
    "  --config-context            Config directory sent to buildkit (Default '.')" \
    "  --target-platforms          Target build platforms (e.g. 'linux/amd64')" \
    "  --publish-image-name        Name under which to publish the image" \
    "  --publish-image-tag         Tag under which to publish the image" \
    "  --publish-image-extra-tags  Extra tags to publish for the image (comma separated)" \
    "" \
    "Environment variables:" \
    "BUILD_CONTEXT               Default for --context" \
    "BUILD_CONFIG                Default for --config" \
    "BUILD_CONFIG_CONTEXT        Default for --config-context" \
    "BULID_TARGET_PLATFORMS      Default for --target-platforms" \
    "PUBLISH_IMAGE_NAME          Default for --publish-image-name" \
    "PUBLISH_IMAGE_TAG           Default for --publish-image-tag" \
    "PUBLISH_IMAGE_EXTRA_TAGS    Default for --publish-image-extra-tags"
}

# Builds an image variant
#
kokkuri_image_build() {
  local opts=(
    context:
    config:
    config-context:
    target-platforms:
    publish-image-name:
    publish-image-tag:
    publish-image-extra-tags:
  )

  local context="${BUILD_CONTEXT:-.}"
  local config="${BUILD_CONFIG:-.pipeline/blubber.yaml}"
  local config_context="${BUILD_CONFIG_CONTEXT:-.}"
  local target_platforms="${BUILD_TARGET_PLATFORMS:-}"
  local publish_image_name="${PUBLISH_IMAGE_NAME:-}"
  local publish_image_tag="${PUBLISH_IMAGE_TAG:-}"
  local publish_image_extra_tags="${PUBLISH_IMAGE_EXTRA_TAGS:-}"
  local variant="${BUILD_VARIANT:-}"

  eval set -- "$(getopt -o "" --long "$(join "," "${opts[@]}")" -- "$@")"
  while true; do
    case "$1" in
      --context) context="$2"; shift 2 ;;
      --config) config="$2"; shift 2 ;;
      --config-context) config_context="$2"; shift 2 ;;
      --target-platforms) target_platforms="$2"; shift 2 ;;
      --publish-image-name) publish_image_name="$2"; shift 2 ;;
      --publish-image-tag) publish_image_tag="$2"; shift 2 ;;
      --publish-image-extra-tags) publish_image_extra_tags="$2"; shift 2 ;;
      --) shift; break ;;
    esac
  done

  case "${1:-}" in
    -*|"")
      if ! [[ "${variant}" ]]; then
        echo "a variant is required"
        kokkuri_image_build_usage
      fi
      ;;
    *)
      variant="$1"
      shift
  esac

  ensure_jwt_auth \
    "${KOKKURI_REGISTRY_INTERNAL}" \
    "${KOKKURI_REGISTRY_PUBLIC}" \
    "${KOKKURI_REGISTRY_CACHE}"

  # shellcheck disable=SC2046
  buildctl ${BUILDCTL_FLAGS} build \
    --progress plain \
    --frontend=gateway.v0 \
    --opt source="$(buildkit_frontend "${config_context}/${config}")" \
    $(buildkit_proxy_options) \
    $(buildkit_caching_options "$variant") \
    --local context="${context}" \
    --local dockerfile="${config_context}" \
    --opt filename="${config}" \
    --opt target="${variant}" \
    $(echo_if "$target_platforms" "--opt" "platform=${target_platforms}") \
    $(buildkit_publish_options "$publish_image_name" "$publish_image_tag" "$publish_image_extra_tags") \
    ${BUILDCTL_BUILD_FLAGS} \
    "$@"

  if [ "$publish_image_name" ]; then
    export_job_variable IMAGE_TAG "$publish_image_tag"
    export_job_variable IMAGE_INTERNAL_REF "$(internal_image_ref "$publish_image_name" "$publish_image_tag")"
    export_job_variable IMAGE_REF "$(public_image_ref "$publish_image_name" "$publish_image_tag")"
  fi
}

# Parse syntax= lines from the image build config. If the user frontend image
# has the same name as ours (minus the tag), it is allowed.
#
buildkit_frontend() {
  local config="$1"

  local frontend="$KOKKURI_DEFAULT_BUILDKIT_FRONTEND"
  local user_frontend=""

  if [ -e "$config" ]; then
    user_frontend="$(awk -F ' *= *' '/^# *syntax *= */ { print $2; next } { exit }' "$config" | tail -n 1)"
  fi

  if [ "${user_frontend}" ]; then
    if [ "${KOKKURI_DEFAULT_BUILDKIT_FRONTEND%:*}" == "${user_frontend%:*}" ]; then
      info "Valid syntax line found in $config"
      frontend="$user_frontend"
    else
      warn "WARNING: Invalid syntax line in $config: build frontend $user_frontend is not allowed"
    fi
  fi

  info "Using build frontend $frontend"

  echo "$frontend"
}


# Check for a caching registry and import/export caches based on the
# repo/branch/variant. Runners in environment that have a caching registry
# should define KOKKURI_REGISTRY_CACHE as the registry host/port.
#
# Import/export refs will be guessed in a heuristic fashion based on CI
# variables to hopefully maximize the possibility of common cache lineage
# between this job/pipeline and previous ones.
#
# When dealing with a merge request, we'll both push and pull using a ref
# based on the MR source branch, and pull using a ref based on the target
# branch.
#
# When dealing with a branch or tag, we'll both push and pull using a ref
# based on the git ref name, and pull using a ref based on the repos default
# branch.
#
buildkit_caching_options() {
  local variant="$1"
  local import_refs=()
  local export_ref=

  if [ "${KOKKURI_REGISTRY_CACHE:-}" ]; then
    info "Registry based caching available at ${KOKKURI_REGISTRY_CACHE_NS}"

    if [ "${CI_MERGE_REQUEST_ID:-}" ]; then
      import_refs+=(
        "$(cache_image_ref "$variant" "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}")"
        "$(cache_image_ref "$variant" "${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}")"
      )
      export_ref="$(cache_image_ref "$variant" "${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}")"
    else
      import_refs+=(
        "$(cache_image_ref "$variant" "${CI_DEFAULT_BRANCH}")"
        "$(cache_image_ref "$variant" "${CI_COMMIT_REF_NAME}")"
      )
      export_ref="$(cache_image_ref "$variant" "${CI_COMMIT_REF_NAME}")"
    fi

    for ref in "${import_refs[@]}"; do
      echo "--import-cache type=registry,ref=${ref}"
    done

    echo "--export-cache type=registry,ref=${export_ref}"
  fi
}

buildkit_publish_options() {
  local name="${1:-}"
  local tag="${2:-}"
  local extra_tags="${3:-}"

  if [ "$name" ]; then
    local -a tags
    IFS=", " read -ra tags <<< "$tag,$extra_tags"

    local refs=

    local tag
    # If no tags are supplied, this still iterates once with
    # a blank value for 'tag', which is what we want.
    for tag in "${tags[@]}"; do
      local ref
      ref="$(internal_image_ref "$name" "$tag")"

      if [ "$refs" ]; then
        refs="$refs,$ref"
      else
        refs="$ref"
      fi
    done

    echo "--output type=image,\"name=$refs\",push=true"
  fi
}

buildkit_proxy_options() {
  if [ "${HTTP_PROXY:-}" ]; then
    debug "HTTP_PROXY set to $HTTP_PROXY"
    echo "--opt build-arg:http_proxy=$HTTP_PROXY"
  fi

  if [ "${HTTPS_PROXY:-}" ]; then
    debug "HTTPS_PROXY set to $HTTPS_PROXY"
    echo "--opt build-arg:https_proxy=$HTTPS_PROXY"
  fi

  if [ "${NO_PROXY:-}" ]; then
    debug "NO_PROXY set to $NO_PROXY"
    echo "--opt build-arg:no_proxy=$NO_PROXY"
  fi
}

image_ref() {
  local namespace="$1"
  local name="$2"
  local tag="${3:-}"
  local clean_tag=

  echo -n "${namespace}/${name}"

  clean_tag="$(image_tag "$tag")"
  if [ "$clean_tag" ]; then
    echo ":${clean_tag}"
  else
    echo ""
  fi
}

image_tag() {
  echo -n "$1" | tr -c 'a-zA-Z0-9_.-' - | cut -c -128
}

cache_image_ref() {
  image_ref "${KOKKURI_REGISTRY_CACHE_NS}" "$@"
}

internal_image_ref() {
  image_ref "${KOKKURI_REGISTRY_INTERNAL}" "$@"
}

public_image_ref() {
  image_ref "${KOKKURI_REGISTRY_PUBLIC}" "$@"
}

# Configures the docker client config to use $CI_JOB_JWT as a bearer token
# for authentication against the given registries
#
ensure_jwt_auth() {
  local config="${DOCKER_CONFIG}/config.json"
  local tmp tmps

  tmp="$(mktemp)"
  tmps=("$tmp")

  mkdir -p "${DOCKER_CONFIG}"
  [ -e "$config" ] || echo "{}" > "$config"

  cp "$config" "$tmp"

  # create a config file for each registry
  for registry in "$@"; do
    if [ "$registry" ]; then
      tmp="$(mktemp)"
      tmps+=("$tmp")

      info "Configuring Docker client to use JWT auth for $registry"

      jq -n --arg registry "$registry" --arg token "$CI_JOB_JWT" \
        '{ auths: { ($registry): { registrytoken: $token } } }' > "$tmp"

      debug "Generated docker config" "$(cat "$tmp")"
    fi
  done

  # merge all the config files together
  debug "Merging Docker configs" "${tmps[@]}"
  jq -s 'reduce .[] as $c ({}; . * $c)' "${tmps[@]}" > "$config"

  debug "Removing temporary configs" "${tmps[@]}"
  rm "${tmps[@]}"
}

kokkuri_image_deploy_usage() {
  usage "image deploy [options] CHART IMAGE IMAGE_TAG" \
        "  --test   Run helm test on the release after deployment"
}

# Deploy an image using helm
#
kokkuri_image_deploy() {
  local chart
  local image
  local image_tag
  local test=
  
  eval set -- "$(getopt -o "" --long "test" -- "$@")"
  while true; do
    case "$1" in
      --test) test=true; shift ;;
      --) shift; break ;;
    esac
  done

  if [ $# -ne 3 ]; then
    kokkuri_image_deploy_usage
  fi

  chart="$1"
  image="$2"
  image_tag="$3"

  local helmTimeout=120s
  local chartRepository="https://helm-charts.wikimedia.org/stable/"
  local namespace=ci
  local registry=docker-registry.wikimedia.org
  local pullPolicy=IfNotPresent
  local version=""

  local release
  local valuesfile

  release=$(cat /proc/sys/kernel/random/uuid)

  valuesfile=$(mktemp)

  cat <<EOF > "$valuesfile"
docker:
  registry: $registry
  pull_policy: $pullPolicy
main_app:
  image: $image
  version: $image_tag
EOF
  
  export KUBECONFIG=$CI_STAGING_KUBECONFIG
  helm install \
       "$release" "$chart" \
       --namespace "$namespace" \
       --values "$valuesfile" \
       --debug --wait \
       --timeout "$helmTimeout" \
       --repo "$chartRepository" \
       "$version"

  if [ "$test" ]; then
    helm test "$release"
  fi

  helm delete "$release"
  export -n KUBECONFIG
  rm "$valuesfile"
}

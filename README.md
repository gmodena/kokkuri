![kokkuri](docs/logo-400.png)

Summon our tricksy CI functions to build/test/publish your WMF deployable artifacts.

## Getting started

This project contains reusable GitLab CI templates and includes that are meant
to help you get your deployable artifacts built, tested, and published for WMF
production deployment.

## Usage

Source files from this project into your own project's `.gitlab-ci.yml` file
using [GitLab CI includes](https://docs.gitlab.com/ee/ci/yaml/includes.html).

## Examples

### Build an image variant

Build an image variant from your [Blubber][blubber] config by including
[includes/images.yaml](./includes/images.yaml) and extending
`.kokkuri:build-image`.

```yaml
include:
  - project: 'repos/releng/kokkuri'
    file: 'includes/images.yaml'

stages:
  - build

build-my-test-variant:
  extends: .kokkuri:build-image
  stage: build
  variables:
    BUILD_VARIANT: test
```

### Run an image variant

Build and run an image variant (such as a test suite) from your
[Blubber][blubber] config using `.kokkuri:build-and-run-image`.

```yaml
include:
  - project: 'repos/releng/kokkuri'
    file: 'includes/images.yaml'

stages:
  - test

run-my-test-variant:
  extends: .kokkuri:build-and-run-image
  stage: test
  variables:
    BUILD_VARIANT: npm-run
    RUN_ARGUMENTS: '["test", "--", "specific.test.js"]'
```

### Publish an image variant

Build and publish a production image variant using
`.kokkuri:build-and-publish-image`.

Typically you'd want to do this each time a protected branch receives a merge
or when a new protected tag is pushed, and by default the image will be tagged
with the git branch/tag.

```yaml
include:
  - project: 'repos/releng/kokkuri'
    file: 'includes/images.yaml'

stages:
  - publish

publish-my-production-variant:
  extends: .kokkuri:build-and-publish-image
  stage: publish
  variables:
    BUILD_VARIANT: production
  tags:
    - protected
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
```

[blubber]: https://wikitech.wikimedia.org/wiki/Blubber

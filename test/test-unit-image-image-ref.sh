#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$LIBDIR"/common.inc.sh
. "$LIBDIR"/image.mod.sh

test_description='Test image functions - image ref functions'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "image_ref" '
  image_ref "our.example/namespace" "foo-name" "foo-tag" \
    test_expect_output "our.example/namespace/foo-name:foo-tag"
'

test_expect_success "internal_image_ref" '
  export KOKKURI_REGISTRY_INTERNAL_NS=our.internal.example/namespace
  internal_image_ref "foo-name" "foo-tag" \
    test_expect_output "our.internal.example/namespace/foo-name:foo-tag"
'

test_expect_success "public_image_ref" '
  export KOKKURI_REGISTRY_PUBLIC_NS=our.public.example/namespace
  public_image_ref "foo-name" "foo-tag" \
    test_expect_output "our.public.example/namespace/foo-name:foo-tag"
'

test_expect_success "cache_image_ref" '
  export KOKKURI_REGISTRY_CACHE_NS=our.cache.example/namespace
  cache_image_ref "foo-name" "foo-tag" \
    test_expect_output "our.cache.example/namespace/foo-name:foo-tag"
'

test_expect_success "image_tag" '
  image_tag "some disallowed/stuff" | \
    test_expect_output "some-disallowed-stuff"
'

test_done

#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$LIBDIR"/common.inc.sh
. "$LIBDIR"/image.mod.sh

test_description='Test image functions - ensure_jwt_auth'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "writes \$CI_JOB_JWT to the docker config for all given registries" "
  export CI_JOB_JWT=foo-jwt-token
  export DOCKER_CONFIG=.docker
  ensure_jwt_auth 'some.example' 'another.example' 'one.more.example'
  test_expect_json \
    '{
      \"auths\": {
        \"some.example\": {
          \"registrytoken\": \"foo-jwt-token\"
        },
        \"another.example\": {
          \"registrytoken\": \"foo-jwt-token\"
        },
        \"one.more.example\": {
          \"registrytoken\": \"foo-jwt-token\"
        }
      }
    }' \
    < .docker/config.json
"

test_done

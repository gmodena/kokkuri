#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$LIBDIR"/common.inc.sh
. "$LIBDIR"/image.mod.sh

test_description='Test image functions - buildkit_publish_options'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "given no name, outputs nothing" '
  export KOKKURI_REGISTRY_INTERNAL=our.example
  buildkit_publish_options "" | test_expect_no_output
'

test_expect_success "given at least a name, outputs publish option" '
  export KOKKURI_REGISTRY_INTERNAL=our.example
  buildkit_publish_options foo-name | \
    test_expect_output \
      "--output type=image,\"name=our.example/foo-name\",push=true"
'

test_expect_success "given a name and tag, outputs publish option with both" '
  export KOKKURI_REGISTRY_INTERNAL=our.example
  buildkit_publish_options foo-name foo-tag | \
    test_expect_output \
      "--output type=image,\"name=our.example/foo-name:foo-tag\",push=true"
'

test_expect_success "given a name, tag, and extra tags, outputs a name option with all of the refs" '
  export KOKKURI_REGISTRY_INTERNAL=our.example
  buildkit_publish_options foo-name foo-tag extra1,extra2 | \
    test_expect_output \
      "--output type=image,\"name=our.example/foo-name:foo-tag,our.example/foo-name:extra1,our.example/foo-name:extra2\",push=true"
'

test_done

#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$DIR"/stub-ci-job-vars.inc.sh
. "$LIBDIR"/common.inc.sh

test_description="Test common functions"

. "$DIR"/lib-sharness.inc.sh

test_expect_success "export_job_variable - writes variables to dotenv file using job name prefix" '
  export_job_variable FOO "foo value" &&
  export_job_variable BAR "bar value" &&
  grep -q "^FOO_JOB_FOO=foo value$" ${KOKKURI_DOTENV_PATH} &&
  grep -q "^FOO_JOB_BAR=bar value$" ${KOKKURI_DOTENV_PATH}
'

test_expect_success "join - joins all arguments by delimiter" '
  test_eq "foo, bar" "$(join ", " "foo" "bar")"
'

test_done

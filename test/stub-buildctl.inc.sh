. "$(dirname "${BASH_SOURCE[0]}")"/lib-helpers.inc.sh

# A stubbed buildctl command that merely echo its arguments for testing.
#
buildctl() {
  stub_command "buildctl" "$@"
}

export -f buildctl

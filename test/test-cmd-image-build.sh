#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
PATH="$(realpath "${DIR}/../bin"):${PATH}"
echo $PATH

. "$DIR"/stub-buildctl.inc.sh

test_description='Test `kokkuri image build`'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "on branch job, no caching" '
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-project-vars.inc.sh
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-job-vars.inc.sh
  KOKKURI_REGISTRY_INTERNAL=registry.example.local \
  KOKKURI_REGISTRY_PUBLIC=registry.example \
  KOKKURI_REGISTRY_CACHE="" \
  kokkuri -qq image build foo | \
    test_expect_command \
      buildctl build \
      --progress plain \
      --frontend=gateway.v0 \
      --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.16.0 \
      --local context=. \
      --local dockerfile=. \
      --opt filename=.pipeline/blubber.yaml \
      --opt target=foo
'

test_expect_success "on branch job, with caching" '
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-project-vars.inc.sh
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-job-vars.inc.sh
  KOKKURI_REGISTRY_INTERNAL=registry.example.local \
  KOKKURI_REGISTRY_PUBLIC=registry.example \
  KOKKURI_REGISTRY_CACHE=registry.cache.example \
  kokkuri -qq image build foo | \
    test_expect_command \
      buildctl build \
      --progress plain \
      --frontend=gateway.v0 \
      --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.16.0 \
      --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-default-branch \
      --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-ref-name \
      --export-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-ref-name \
      --local context=. \
      --local dockerfile=. \
      --opt filename=.pipeline/blubber.yaml \
      --opt target=foo
'

test_expect_success "on merge request branch, with caching" '
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-project-vars.inc.sh
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-job-vars.inc.sh
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-merge-request-vars.inc.sh
  KOKKURI_REGISTRY_INTERNAL=registry.example.local \
  KOKKURI_REGISTRY_PUBLIC=registry.example \
  KOKKURI_REGISTRY_CACHE=registry.cache.example \
  kokkuri -qq image build foo | \
    test_expect_command \
      buildctl build \
      --progress plain \
      --frontend=gateway.v0 \
      --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.16.0 \
      --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-mr-target-branch \
      --import-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-mr-source-branch \
      --export-cache type=registry,ref=registry.cache.example/foo/project/cache/foo:foo-mr-source-branch \
      --local context=. \
      --local dockerfile=. \
      --opt filename=.pipeline/blubber.yaml \
      --opt target=foo
'

test_expect_success "excepts extra buildctl options" '
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-project-vars.inc.sh
  . "$SHARNESS_TEST_SRCDIR"/stub-ci-job-vars.inc.sh
  kokkuri -qq image build foo -- --opt some=foo | \
    test_expect_command \
      buildctl build \
      --progress plain \
      --frontend=gateway.v0 \
      --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.16.0 \
      --local context=. \
      --local dockerfile=. \
      --opt filename=.pipeline/blubber.yaml \
      --opt target=foo \
      --opt some=foo
'

test_done

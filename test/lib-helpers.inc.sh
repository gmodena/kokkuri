#!/bin/bash

echo_each() {
  for arg in "$@"; do
    echo "$arg"
  done
}

echo_as_json() {
  echo_each "$@" | jq --raw-input --slurp -c 'split("\n")[0:-1]'
}

stub_command() {
  echo_as_json "$@"
}

test_expect_command() {
  local cmd
  cmd="$(echo_as_json "$@")"
  test_expect_json "$cmd" "command" "command.json"
}

test_expect_json() {
  local expected="$1"
  local label="${2:-json}"
  local ext="${3:-json}"
  local actual_path
  local expected_path
  local ret

  actual_path=".test.actual.${ext}"
  expected_path=".test.expected.${ext}"

  cat > "$actual_path"
  echo "$expected" > "$expected_path"

  jq --slurp -e '.[0] == .[1]' "$expected_path" "$actual_path"
  ret="$?"

  if [[ $ret -gt 0 ]]; then
    echo "Expected ${label}: $expected"
    echo "Actual: $(cat "$actual_path")"
    echo "Diff:"
    diff -u <(jq --sort-keys . "$expected_path") <(jq --sort-keys . "$actual_path")
  fi

  return $ret
}

test_eq() {
  local expected="$1"
  local actual="$2"
  local ret

  [[ "$expected" == "$actual" ]]
  ret="$?"

  if [[ $ret -gt 0 ]]; then
    echo "Expected: $expected"
    echo "Actual: $actual"

    diff -u <(echo "$expected") <(echo "$actual")
  fi

  return $ret
}

test_expect_output() {
  local expected=".test.expected.output"
  local actual=".test.actual.output"
  local ret

  trap "rm -f ${expected@Q} ${actual@Q}" RETURN

  cat > "$actual"
  touch "$expected"

  for line in "$@"; do
    echo "$line" >> "$expected"
  done

  test_cmp "$expected" "$actual"
}

test_expect_no_output() {
  test_expect_output
}

export -f \
  echo_each \
  echo_as_json \
  stub_command \
  test_expect_command \
  test_expect_json \
  test_eq \
  test_expect_output \
  test_expect_no_output

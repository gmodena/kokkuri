#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$DIR"/stub-ci-job-vars.inc.sh
. "$LIBDIR"/common.inc.sh
. "$LIBDIR"/image.mod.sh

test_description='Test image functions - buildkit_frontend'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "with no user frontend, uses default frontend" '
  echo "version: v4" > blubber.yaml
  export KOKKURI_DEFAULT_BUILDKIT_FRONTEND=our.example/frontend:v1.2.3
  test_eq "our.example/frontend:v1.2.3" "$(buildkit_frontend blubber.yaml)"
'

test_expect_success "respects user frontend if all but tags match" '
  echo "# syntax=our.example/frontend:v1.1.0" > blubber.yaml
  echo "version: v4" >> blubber.yaml
  export KOKKURI_DEFAULT_BUILDKIT_FRONTEND=our.example/frontend:v1.2.3
  test_eq "our.example/frontend:v1.1.0" "$(buildkit_frontend blubber.yaml)"
'

test_expect_success "with different user frontend, uses default frontend" '
  echo "# syntax=their.example/frontend:v0.0.1" > blubber.yaml
  echo "build stuff" >> blubber.yaml
  export KOKKURI_DEFAULT_BUILDKIT_FRONTEND=our.example/frontend:v1.2.3
  test_eq "our.example/frontend:v1.2.3" "$(buildkit_frontend blubber.yaml)"
'

test_done

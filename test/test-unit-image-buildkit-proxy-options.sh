#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$LIBDIR"/common.inc.sh
. "$LIBDIR"/image.mod.sh

test_description='Test image functions - buildkit_proxy_options'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "variables set, outputs build-arg options" '
  export HTTP_PROXY=http://our.example
  export HTTPS_PROXY=https://our.example
  export NO_PROXY=http://foo.example
  buildkit_proxy_options | \
    test_expect_output \
      "--opt build-arg:http_proxy=http://our.example" \
      "--opt build-arg:https_proxy=https://our.example" \
      "--opt build-arg:no_proxy=http://foo.example"
'

test_expect_success "variables not set, outputs nothing" '
  unset HTTP_PROXY
  unset HTTPS_PROXY
  unset NO_PROXY
  buildkit_proxy_options | test_expect_no_output
'

test_done

#!/bin/bash

set -o pipefail

DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
LIBDIR="$(realpath "${DIR}/../lib/kokkuri")"

. "$DIR"/lib-helpers.inc.sh
. "$DIR"/stub-ci-project-vars.inc.sh
. "$DIR"/stub-ci-job-vars.inc.sh
. "$LIBDIR"/common.inc.sh
. "$LIBDIR"/image.mod.sh

test_description='Test image functions - buildkit_caching_options'

. "$DIR"/lib-sharness.inc.sh

test_expect_success "without cache registry, outputs nothing" '
  test_eq "" "$(buildkit_caching_options foo-variant)"
'

test_expect_success "with cache registry, on branch/tag" '
  export KOKKURI_REGISTRY_CACHE=our.cache.example
  buildkit_caching_options foo-variant | \
    test_expect_output \
      "--import-cache type=registry,ref=/foo/project/cache/foo-variant:foo-default-branch" \
      "--import-cache type=registry,ref=/foo/project/cache/foo-variant:foo-ref-name" \
      "--export-cache type=registry,ref=/foo/project/cache/foo-variant:foo-ref-name"
'

test_expect_success "with cache registry, on merge request branch" '
  export KOKKURI_REGISTRY_CACHE=our.cache.example
  . "${SHARNESS_TEST_SRCDIR}/stub-ci-merge-request-vars.inc.sh"
  buildkit_caching_options foo-variant | \
    test_expect_output \
      "--import-cache type=registry,ref=/foo/project/cache/foo-variant:foo-mr-target-branch" \
      "--import-cache type=registry,ref=/foo/project/cache/foo-variant:foo-mr-source-branch" \
      "--export-cache type=registry,ref=/foo/project/cache/foo-variant:foo-mr-source-branch"
'

test_done
